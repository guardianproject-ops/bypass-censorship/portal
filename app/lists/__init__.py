from typing import Any, Callable, Dict

from app.lists.bc2 import mirror_sites
from app.lists.bridgelines import bridgelines
from app.lists.mirror_mapping import mirror_mapping
from app.lists.redirector import redirector_data
from app.models.base import Pool

lists: Dict[str, Callable[[Pool], Any]] = {
    "bca": mirror_mapping,
    "bc2": mirror_sites,
    "bridgelines": bridgelines,
    "rdr": redirector_data,
}
