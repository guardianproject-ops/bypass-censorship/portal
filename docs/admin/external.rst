External Services
=================

Overview
--------

In order to deploy the circumvention resources, the following providers can be used:

+------------+-----+-------+---------+---------+----------------+-----+
| Resource   | AWS | Azure | Fastly  | Hetzner | GandiCloud VPS | OVH |
+============+=====+=======+=========+=========+================+=====+
| Web Proxy  | ✅  | ✅    | ✅      | ❌      | ❌             | ❌  |
+------------+-----+-------+---------+---------+----------------+-----+
| Tor Bridge | ✅  | ❌    | ❌      | ✅      | ✅             | ✅  |
+------------+-----+-------+---------+---------+----------------+-----+

Circumvention resource lists can be distributed via:

* AWS S3
* GitHub
* GitLab
* HTTP POST

AWS
---

Configuring AWS will require an
`IAM account <https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html>`_
with programmatic access.
As the portal may deploy resources using many AWS services it is currently
It is recommended that a dedicated account is used for the deployement of portal managed resources.
The dedicated account may be part of an existing AWS organisation.

.. code-block:: yaml

   # Amazon Web Services
   AWS_ACTIVATED: true
   AWS_ACCESS_KEY: AKIAIOSFODNN7EXAMPLE
   AWS_SECRET_KEY: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY

Route 53
^^^^^^^^

Some resources that can be deployed via the portal, e.g. mirrors, rely on DNS names to map internal connections.
These DNS names are only used within AWS and are never accessed directly by end users.
A registered domain must be used for this purpose.

Once a domain name has been registered,
`create a hosted zone <https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/CreatingHostedZone.html>`_
within Route 53.
Ensure that the domain registrar is updated to point to the Route 53 nameservers.
Optionally, enable DNSSEC to improve the security of the hosted zone.
Add the domain name, including the terminating period, to the configuration for the portal:

.. code-block:: yaml

   SMART_ZONE: example.com.

.. warning::

   Do not create any other records on this domain manually as these may conflict with names created by the portal.

GitHub
------

To configure GitHub, you will need a "personal access token".
It is not possible to restrict a token to specific repositories, so it is best
to create a
`machine user <https://docs.github.com/en/developers/overview/managing-deploy-keys#machine-users>`_
and then invite that user to only the repositories that should be accessed to
limit the impact of a token disclosure.

To create a token, visit the `New personal access token <https://github.com/settings/tokens/new>`_
page of the GitHub settings.
Add a note so that you will be reminded of the purpose of this token when you go to look at it later.
The expiry can be set according to your threat model.
GitHub will
`send an email warning <https://github.blog/changelog/2021-07-26-expiration-options-for-personal-access-tokens/>`_
before the token expires allowing you to generate a new token and update your configuration.

Your access token will need the "repo" OAuth scope. If you are only using public repos, you could limit the token
to use the "public_repo" scope.

Once you've generated your token, you can add it to your ``config.yaml``:

.. code-block:: yaml

   # GitHub
   GITHUB_ACTIVATED: true
   GITHUB_API_KEY: ghp_Sha8ShueNgihibai6soo1ojoo4aez0deo3bo

The organisation, repository, filename and formats are all configured via the portal interface under
:doc:`mirror lists <../user/lists>`.

GitLab
------

To configure GitLab, you will need an access token.
GitLab has a variety of scoped access tokens, including
`personal <https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html>`_,
`group <https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html>`_
and
`project <https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html>`_
access tokens.
Create the type of token that is suitable according to your threat model.

If you are using a personal access token, the user must have at least the *Maintainer*
role for the project.

Add a name so that you will be reminded of the purpose of this token when you go
to look at it later.
The expiry can be set according to your threat model.
GitLab will send an email warning to token owners 7 days before expiry
allowing you to generate a new token and update your configuration.

Your access token will need the "api" scope. Unforunately the "write_repository" scope
only works for Git-over-HTTPS, but the portal uses the API to update mirror lists in
GitLab.

Once you've generated your token, you can add it to your ``config.yaml``:

.. code-block:: yaml

   # GitLab
   GITLAB_ACTIVATED: true
   GITLAB_TOKEN: glpat-keiyahtuup7oewee7Bie

The organisation, repository, filename and formats are all configured via the portal interface under
:doc:`mirror lists <../user/lists>`.

OVH
---

OVH requires two different sets of credentials.
The first is for the OpenStack Horizon API, and the second for the OVH API.

OVH provides documentation on creating the OpenStack Horizon user
`here <https://docs.ovh.com/gb/en/public-cloud/creation-and-deletion-of-openstack-user/#creating-an-openstack-user>`_.
As the portal will manage all resources within your OVH project, it is fine to
allow for the user to have "Administrator" access.

Once you have the username and password, you can complete the first fields of the
configuration:

.. code-block:: yaml

   # OVH Cloud
   OVH_ACTIVATED: true
   OVH_OPENSTACK_USER: user-TUNqsurjzZq8
   OVH_OPENSTACK_PASSWORD: Dq8t7fDNY2JaT8qKnQG25fQGKPfTf4ef

You can find the tenant ID in the top-left corner of the OVH cloud dashboard, or in the
`OpenStack RC file <https://docs.ovh.com/gb/en/public-cloud/set-openstack-environment-variables/>`_
available for download in the Horizon interface.

.. code-block:: yaml

   OVH_OPENSTACK_TENANT_ID: 813a2746edfa22856f002bc40191e340

Finally, you'll need to create credentials for the OVH API.
Go to the `key creation page <https://eu.api.ovh.com/createToken/>`_ and add an
application name and description to remind you later what the token is for.
Set a validity (expiry) according to your threat model.
This token is only used for read-only access, so it is sufficient to allow only
GET requests.
Place an asterisk (*) in the text box to the right of GET so that
API paths are allowed.

.. code-block:: yaml

   OVH_CLOUD_APPLICATION_KEY: 86597b977f13f9cf
   OVH_CLOUD_APPLICATION_SECRET: 351596d82649ffdbcfa55ea93208e358
   OVH_CLOUD_CONSUMER_KEY: 07d8da20bca15481d23ce7f3f5fbe80b
