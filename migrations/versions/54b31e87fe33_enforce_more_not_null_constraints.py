"""enforce more not null constraints

Revision ID: 54b31e87fe33
Revises: c14f25f364c5
Create Date: 2024-12-06 16:06:59.182836

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '54b31e87fe33'
down_revision = 'c14f25f364c5'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('activity', schema=None) as batch_op:
        batch_op.alter_column('text',
               existing_type=sa.TEXT(),
               type_=sa.String(),
               existing_nullable=False)

    with op.batch_alter_table('automation_logs', schema=None) as batch_op:
        batch_op.alter_column('logs',
               existing_type=sa.TEXT(),
               type_=sa.String(),
               nullable=False)

    with op.batch_alter_table('bridge', schema=None) as batch_op:
        batch_op.alter_column('cloud_account_id',
               existing_type=sa.INTEGER(),
               nullable=False)

    with op.batch_alter_table('mirror_list', schema=None) as batch_op:
        batch_op.alter_column('pool_id',
               existing_type=sa.INTEGER(),
               nullable=False)

    with op.batch_alter_table('webhook', schema=None) as batch_op:
        batch_op.alter_column('format',
               existing_type=sa.VARCHAR(length=20),
               nullable=False)
        batch_op.alter_column('url',
               existing_type=sa.VARCHAR(length=255),
               nullable=False)


def downgrade():
    with op.batch_alter_table('webhook', schema=None) as batch_op:
        batch_op.alter_column('url',
               existing_type=sa.VARCHAR(length=255),
               nullable=True)
        batch_op.alter_column('format',
               existing_type=sa.VARCHAR(length=20),
               nullable=True)

    with op.batch_alter_table('mirror_list', schema=None) as batch_op:
        batch_op.alter_column('pool_id',
               existing_type=sa.INTEGER(),
               nullable=True)

    with op.batch_alter_table('bridge', schema=None) as batch_op:
        batch_op.alter_column('cloud_account_id',
               existing_type=sa.INTEGER(),
               nullable=True)

    with op.batch_alter_table('automation_logs', schema=None) as batch_op:
        batch_op.alter_column('logs',
               existing_type=sa.String(),
               type_=sa.TEXT(),
               nullable=True)

    with op.batch_alter_table('activity', schema=None) as batch_op:
        batch_op.alter_column('text',
               existing_type=sa.String(),
               type_=sa.TEXT(),
               existing_nullable=False)

