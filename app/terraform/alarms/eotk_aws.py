from typing import Optional, Tuple

import boto3
from sqlalchemy import func

from app import app
from app.alarms import get_or_create_alarm
from app.extensions import db
from app.models.alarms import AlarmState
from app.models.base import Group
from app.models.onions import Eotk
from app.terraform import BaseAutomation


def alarms_in_region(region: str, prefix: str, aspect: str) -> None:
    cloudwatch = boto3.client(
        "cloudwatch",
        aws_access_key_id=app.config["AWS_ACCESS_KEY"],
        aws_secret_access_key=app.config["AWS_SECRET_KEY"],
        region_name=region,
    )
    dist_paginator = cloudwatch.get_paginator("describe_alarms")
    page_iterator = dist_paginator.paginate(AlarmNamePrefix=prefix)
    for page in page_iterator:
        for cw_alarm in page["MetricAlarms"]:
            eotk_id = cw_alarm["AlarmName"][len(prefix) :].split("-")
            group: Optional[Group] = Group.query.filter(
                func.lower(Group.group_name) == eotk_id[1]
            ).first()
            if group is None:
                print("Unable to find group for " + cw_alarm["AlarmName"])
                continue
            eotk = Eotk.query.filter(
                Eotk.group_id == group.id, Eotk.region == region
            ).first()
            if eotk is None:
                print("Skipping unknown instance " + cw_alarm["AlarmName"])
                continue
            alarm = get_or_create_alarm(eotk.brn, aspect)
            if cw_alarm["StateValue"] == "OK":
                alarm.update_state(AlarmState.OK, "CloudWatch alarm OK")
            elif cw_alarm["StateValue"] == "ALARM":
                alarm.update_state(AlarmState.CRITICAL, "CloudWatch alarm ALARM")
            else:
                alarm.update_state(
                    AlarmState.UNKNOWN, f"CloudWatch alarm {cw_alarm['StateValue']}"
                )


class AlarmEotkAwsAutomation(BaseAutomation):
    short_name = "monitor_eotk_aws"
    description = "Import alarms for AWS EOTK instances"

    def automate(self, full: bool = False) -> Tuple[bool, str]:
        for region in ["us-east-2", "eu-central-1"]:
            alarms_in_region(region, "eotk-bw-out-high-", "bandwidth-out-high")
            alarms_in_region(region, "eotk-cpu-high-", "instance-cpu")
        db.session.commit()
        return True, ""
