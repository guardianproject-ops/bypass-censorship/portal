import jinja2
from flask import current_app

from app.models.base import Group

EOTK_CONFIG_TEMPLATE = """
set log_separate 1

set nginx_resolver 127.0.0.53 ipv6=off

set nginx_cache_seconds 60
set nginx_cache_size 64m
set nginx_tmpfile_size 8m

set x_from_onion_value 1
set inject_headers_upstream Bypass-Rate-Limit-Token,{{ bypass_token }}

foreignmap facebookwkhpilnemxj7asaniu7vnjjbiltxjqhye3mhbshg7kx5tfyd.onion facebook.com
foreignmap twitter3e4tixl4xyajtrzo62zg5vztmjuricljdp2c5kshju4avyoid.onion twitter.com

set project sites
{% for o in group.onions %}
hardmap {{ o.onion_name }} {{ o.domain_name }}
{%- endfor %}
"""


def eotk_configuration(group: Group) -> str:
    """
    Generate an EOTK project configuration for an origin group.

    :param group: the origin group
    :return: the configuration
    """
    tmpl = jinja2.Template(EOTK_CONFIG_TEMPLATE)
    return tmpl.render(bypass_token=current_app.config["BYPASS_TOKEN"], group=group)
