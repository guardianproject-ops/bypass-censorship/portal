FROM debian:bookworm AS portal

ENV APP="bc"
ENV APP_BASE="/srv"
ENV SHELL="/bin/bash"
ENV FLASK_APP="${FLASK_APP:-app}"
ENV WAITRESS_RUN_HOST="${WAITRESS_RUN_HOST:-0.0.0.0}"
ENV WAITRESS_RUN_PORT="${WAITRESS_RUN_PORT:-5000}"
ENV PYTHONPATH="${APP_BASE}/env/lib/python3.11/site-packages"
ENV PATH="${APP_BASE}/env/bin:/usr/local/bin:/usr/bin:/bin:/sbin:/usr/sbin:/home/${APP}/.local/bin"

ARG CONTAINER_UID="${CONTAINER_UID:-1000}"
ARG CONTAINER_GID="${CONTAINER_GID:-1000}"

ENV WAITRESS_THREADS="${WAITRESS_THREADS:-4}"

RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    curl \
    python3-pip \
    python3-venv \
    git \
    gnupg && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ARG OPENTOFU_VERSION="1.8.5"
RUN curl -fsSL https://get.opentofu.org/opentofu.gpg -o opentofu.gpg && \
    gpg --import opentofu.gpg && \
    curl -fsSL https://github.com/opentofu/opentofu/releases/download/v${OPENTOFU_VERSION}/tofu_${OPENTOFU_VERSION}_linux_amd64.tar.gz -o opentofu.tar.gz && \
    curl -fsSL https://github.com/opentofu/opentofu/releases/download/v${OPENTOFU_VERSION}/tofu_${OPENTOFU_VERSION}_linux_amd64.tar.gz.gpgsig -o opentofu.tar.gz.gpgsig && \
    gpg --verify opentofu.tar.gz.gpgsig opentofu.tar.gz && \
    tar -xzf opentofu.tar.gz -C /usr/local/bin tofu && \
    chmod +x /usr/local/bin/tofu && \
    ln -s /usr/local/bin/tofu /usr/local/bin/terraform && \
    rm -rf opentofu.tar.gz opentofu.tar.gz.gpgsig opentofu.gpg /tmp/* /var/tmp/*

RUN apt-get update && \
    apt-get install --no-install-recommends -y curl make && \
    curl -fsSL https://deb.nodesource.com/setup_20.x | bash - && \
    apt-get install -y nodejs && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN groupadd -r -g ${CONTAINER_GID} ${APP} && \
    useradd --no-log-init -r -u ${CONTAINER_UID} -g ${APP} ${APP} && \
    mkdir -p /home/${APP} && chown -R ${APP}:${APP} /home/${APP} && \
    mkdir -p ${APP_BASE}/${APP} ${APP_BASE}/env && chown ${APP}:${APP} ${APP_BASE}/${APP} ${APP_BASE}/env

USER ${APP}

WORKDIR ${APP_BASE}/${APP}
COPY --chown=${APP}:${APP} . ${APP_BASE}/${APP}

RUN python3 -m venv ${APP_BASE}/env && \
    ${APP_BASE}/env/bin/pip install --no-cache-dir -r requirements.txt && \
    ${APP_BASE}/env/bin/pip install --no-cache-dir psycopg2-binary waitress paste
RUN make install-frontend
ENTRYPOINT ["python", "./run_waitress.py"]