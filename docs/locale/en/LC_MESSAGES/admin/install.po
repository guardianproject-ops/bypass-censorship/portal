# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2021-202r SR2 Communications Limited
# This file is distributed under the same license as the Bypass Censorship
# Portal package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2024.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Bypass Censorship Portal \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-12-05 15:12+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language: en\n"
"Language-Team: en <LL@li.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.16.0\n"

#: ../../admin/install.rst:2
msgid "Installation"
msgstr ""

#: ../../admin/install.rst:4
msgid ""
"The current installation method is to use a Python virtual envrionment. "
"Many of the dependencies needed by the portal can be installed from PyPI,"
" although some dependencies will need to be satisfied in other ways:"
msgstr ""

#: ../../admin/install.rst:8
msgid "A cron daemon - probably available in your system package manager"
msgstr ""

#: ../../admin/install.rst:9
msgid "Terraform binary - static binaries available from Hashicorp"
msgstr ""

#: ../../admin/install.rst:10
msgid "PostgreSQL server - probably available in your system package manager"
msgstr ""

#: ../../admin/install.rst:12
msgid "Create and activate a virtual environment with:"
msgstr ""

#: ../../admin/install.rst:20
msgid "You will need to install an entry into the portal user's crontab::"
msgstr ""

