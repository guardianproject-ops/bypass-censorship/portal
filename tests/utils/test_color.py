import pytest

from app.brm.utils import normalize_color


def test_normalize_color():
    # Test valid CSS color names
    assert normalize_color('red') == '#ff0000'
    assert normalize_color('white') == '#ffffff'
    assert normalize_color('black') == '#000000'
    # Test valid 6-digit hex color codes
    assert normalize_color('#ffffff') == '#ffffff'
    assert normalize_color('#000000') == '#000000'
    assert normalize_color('#00ff00') == '#00ff00'
    # Test valid 3-digit hex color codes
    assert normalize_color('#fff') == '#fff'
    assert normalize_color('#000') == '#000'
    assert normalize_color('#0f0') == '#0f0'
    # Test case insensitivity
    assert normalize_color('#FFFFFF') == '#ffffff'
    assert normalize_color('#ABCDEF') == '#abcdef'
    # Test invalid color values
    with pytest.raises(ValueError):
        normalize_color('invalid')
    with pytest.raises(ValueError):
        normalize_color('#1234567')
    with pytest.raises(ValueError):
        normalize_color('#xyz')