"""initial schema

Revision ID: 6a59928efeb7
Revises: 
Create Date: 2022-11-13 19:03:10.787978

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6a59928efeb7'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('activity',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('group_id', sa.Integer(), nullable=True),
    sa.Column('activity_type', sa.String(length=20), nullable=False),
    sa.Column('text', sa.Text(), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_activity'))
    )
    op.create_table('alarm',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('target', sa.String(length=255), nullable=False),
    sa.Column('aspect', sa.String(length=255), nullable=False),
    sa.Column('alarm_state', sa.Enum('UNKNOWN', 'OK', 'WARNING', 'CRITICAL', name='alarmstate'), nullable=False),
    sa.Column('state_changed', sa.DateTime(), nullable=False),
    sa.Column('last_updated', sa.DateTime(), nullable=False),
    sa.Column('text', sa.String(length=255), nullable=False),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_alarm'))
    )
    op.create_table('automation',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('description', sa.String(length=255), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=False),
    sa.Column('destroyed', sa.DateTime(), nullable=True),
    sa.Column('short_name', sa.String(length=25), nullable=False),
    sa.Column('state', sa.Enum('IDLE', 'RUNNING', 'ERROR', name='automationstate'), nullable=False),
    sa.Column('enabled', sa.Boolean(), nullable=False),
    sa.Column('last_run', sa.DateTime(), nullable=True),
    sa.Column('next_run', sa.DateTime(), nullable=True),
    sa.Column('next_is_full', sa.Boolean(), nullable=False),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_automation'))
    )
    op.create_table('group',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('description', sa.String(length=255), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=False),
    sa.Column('destroyed', sa.DateTime(), nullable=True),
    sa.Column('group_name', sa.String(length=80), nullable=False),
    sa.Column('eotk', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_group')),
    sa.UniqueConstraint('group_name', name=op.f('uq_group_group_name'))
    )
    op.create_table('pool',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('description', sa.String(length=255), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=False),
    sa.Column('destroyed', sa.DateTime(), nullable=True),
    sa.Column('pool_name', sa.String(length=80), nullable=False),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_pool')),
    sa.UniqueConstraint('pool_name', name=op.f('uq_pool_pool_name'))
    )
    op.create_table('terraform_state',
    sa.Column('key', sa.String(), nullable=False),
    sa.Column('state', sa.String(), nullable=True),
    sa.Column('lock', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('key', name=op.f('pk_terraform_state'))
    )
    op.create_table('webhook',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('description', sa.String(length=255), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=False),
    sa.Column('destroyed', sa.DateTime(), nullable=True),
    sa.Column('format', sa.String(length=20), nullable=True),
    sa.Column('url', sa.String(length=255), nullable=True),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_webhook'))
    )
    op.create_table('automation_logs',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=False),
    sa.Column('deprecated', sa.DateTime(), nullable=True),
    sa.Column('deprecation_reason', sa.String(), nullable=True),
    sa.Column('destroyed', sa.DateTime(), nullable=True),
    sa.Column('automation_id', sa.Integer(), nullable=False),
    sa.Column('logs', sa.Text(), nullable=True),
    sa.ForeignKeyConstraint(['automation_id'], ['automation.id'], name=op.f('fk_automation_logs_automation_id_automation')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_automation_logs'))
    )
    op.create_table('bridge_conf',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('description', sa.String(length=255), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=False),
    sa.Column('destroyed', sa.DateTime(), nullable=True),
    sa.Column('group_id', sa.Integer(), nullable=False),
    sa.Column('provider', sa.String(length=20), nullable=False),
    sa.Column('method', sa.String(length=20), nullable=False),
    sa.Column('number', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['group_id'], ['group.id'], name=op.f('fk_bridge_conf_group_id_group')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_bridge_conf'))
    )
    op.create_table('eotk',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=False),
    sa.Column('deprecated', sa.DateTime(), nullable=True),
    sa.Column('deprecation_reason', sa.String(), nullable=True),
    sa.Column('destroyed', sa.DateTime(), nullable=True),
    sa.Column('group_id', sa.Integer(), nullable=False),
    sa.Column('instance_id', sa.String(length=100), nullable=True),
    sa.Column('provider', sa.String(length=20), nullable=False),
    sa.Column('region', sa.String(length=20), nullable=False),
    sa.ForeignKeyConstraint(['group_id'], ['group.id'], name=op.f('fk_eotk_group_id_group')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_eotk'))
    )
    op.create_table('mirror_list',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('description', sa.String(length=255), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=False),
    sa.Column('destroyed', sa.DateTime(), nullable=True),
    sa.Column('pool_id', sa.Integer(), nullable=True),
    sa.Column('provider', sa.String(length=255), nullable=False),
    sa.Column('format', sa.String(length=20), nullable=False),
    sa.Column('encoding', sa.String(length=20), nullable=False),
    sa.Column('container', sa.String(length=255), nullable=False),
    sa.Column('branch', sa.String(length=255), nullable=False),
    sa.Column('role', sa.String(length=255), nullable=True),
    sa.Column('filename', sa.String(length=255), nullable=False),
    sa.ForeignKeyConstraint(['pool_id'], ['pool.id'], name=op.f('fk_mirror_list_pool_id_pool')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_mirror_list'))
    )
    op.create_table('onion',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('description', sa.String(length=255), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=False),
    sa.Column('destroyed', sa.DateTime(), nullable=True),
    sa.Column('group_id', sa.Integer(), nullable=False),
    sa.Column('domain_name', sa.String(length=255), nullable=False),
    sa.Column('onion_public_key', sa.LargeBinary(), nullable=False),
    sa.Column('onion_private_key', sa.LargeBinary(), nullable=False),
    sa.Column('tls_public_key', sa.LargeBinary(), nullable=False),
    sa.Column('tls_private_key', sa.LargeBinary(), nullable=False),
    sa.ForeignKeyConstraint(['group_id'], ['group.id'], name=op.f('fk_onion_group_id_group')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_onion'))
    )
    op.create_table('origin',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('description', sa.String(length=255), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=False),
    sa.Column('destroyed', sa.DateTime(), nullable=True),
    sa.Column('group_id', sa.Integer(), nullable=False),
    sa.Column('domain_name', sa.String(length=255), nullable=False),
    sa.Column('auto_rotation', sa.Boolean(), nullable=False),
    sa.Column('smart', sa.Boolean(), nullable=False),
    sa.Column('assets', sa.Boolean(), nullable=False),
    sa.ForeignKeyConstraint(['group_id'], ['group.id'], name=op.f('fk_origin_group_id_group')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_origin')),
    sa.UniqueConstraint('domain_name', name=op.f('uq_origin_domain_name'))
    )
    op.create_table('pool_group',
    sa.Column('pool_id', sa.Integer(), nullable=False),
    sa.Column('group_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['group_id'], ['group.id'], name=op.f('fk_pool_group_group_id_group')),
    sa.ForeignKeyConstraint(['pool_id'], ['pool.id'], name=op.f('fk_pool_group_pool_id_pool')),
    sa.PrimaryKeyConstraint('pool_id', 'group_id', name=op.f('pk_pool_group'))
    )
    op.create_table('smart_proxy',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=False),
    sa.Column('deprecated', sa.DateTime(), nullable=True),
    sa.Column('deprecation_reason', sa.String(), nullable=True),
    sa.Column('destroyed', sa.DateTime(), nullable=True),
    sa.Column('group_id', sa.Integer(), nullable=False),
    sa.Column('instance_id', sa.String(length=100), nullable=True),
    sa.Column('provider', sa.String(length=20), nullable=False),
    sa.Column('region', sa.String(length=20), nullable=False),
    sa.ForeignKeyConstraint(['group_id'], ['group.id'], name=op.f('fk_smart_proxy_group_id_group')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_smart_proxy'))
    )
    op.create_table('bridge',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=False),
    sa.Column('deprecated', sa.DateTime(), nullable=True),
    sa.Column('deprecation_reason', sa.String(), nullable=True),
    sa.Column('destroyed', sa.DateTime(), nullable=True),
    sa.Column('conf_id', sa.Integer(), nullable=False),
    sa.Column('terraform_updated', sa.DateTime(), nullable=True),
    sa.Column('nickname', sa.String(length=255), nullable=True),
    sa.Column('fingerprint', sa.String(length=255), nullable=True),
    sa.Column('hashed_fingerprint', sa.String(length=255), nullable=True),
    sa.Column('bridgeline', sa.String(length=255), nullable=True),
    sa.ForeignKeyConstraint(['conf_id'], ['bridge_conf.id'], name=op.f('fk_bridge_conf_id_bridge_conf')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_bridge'))
    )
    op.create_table('proxy',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=False),
    sa.Column('deprecated', sa.DateTime(), nullable=True),
    sa.Column('deprecation_reason', sa.String(), nullable=True),
    sa.Column('destroyed', sa.DateTime(), nullable=True),
    sa.Column('origin_id', sa.Integer(), nullable=False),
    sa.Column('pool_id', sa.Integer(), nullable=True),
    sa.Column('provider', sa.String(length=20), nullable=False),
    sa.Column('psg', sa.Integer(), nullable=True),
    sa.Column('slug', sa.String(length=20), nullable=True),
    sa.Column('terraform_updated', sa.DateTime(), nullable=True),
    sa.Column('url', sa.String(length=255), nullable=True),
    sa.ForeignKeyConstraint(['origin_id'], ['origin.id'], name=op.f('fk_proxy_origin_id_origin')),
    sa.ForeignKeyConstraint(['pool_id'], ['pool.id'], name=op.f('fk_proxy_pool_id_pool')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_proxy'))
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('proxy')
    op.drop_table('bridge')
    op.drop_table('smart_proxy')
    op.drop_table('pool_group')
    op.drop_table('origin')
    op.drop_table('onion')
    op.drop_table('mirror_list')
    op.drop_table('eotk')
    op.drop_table('bridge_conf')
    op.drop_table('automation_logs')
    op.drop_table('webhook')
    op.drop_table('terraform_state')
    op.drop_table('pool')
    op.drop_table('group')
    op.drop_table('automation')
    op.drop_table('alarm')
    op.drop_table('activity')
    # ### end Alembic commands ###
