import base64
import hashlib
from typing import Dict, List, Optional, Tuple


def onion_hostname(onion_public_key: bytes) -> str:
    p = onion_public_key[32:]

    h = hashlib.sha3_256()
    h.update(b".onion checksum")
    h.update(p)
    h.update(b"\x03")
    checksum = h.digest()

    result = bytearray(p)
    result.extend(checksum[0:2])
    result.append(0x03)

    onion = base64.b32encode(result).decode("utf-8").strip("=")
    return onion.lower()


def decode_onion_keys(
    onion_private_key_base64: str, onion_public_key_base64: str
) -> Tuple[Optional[bytes], Optional[bytes], List[Dict[str, str]]]:
    try:
        onion_private_key = base64.b64decode(onion_private_key_base64)
        onion_public_key = base64.b64decode(onion_public_key_base64)
        return onion_private_key, onion_public_key, []
    except ValueError as e:
        return None, None, [{"Error": "invalid_onion_key", "Message": str(e)}]
