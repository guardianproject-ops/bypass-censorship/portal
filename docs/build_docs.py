import subprocess
import os
import yaml

with open('languages.yaml', 'r') as file:
    config = yaml.safe_load(file)
    languages = config.get('languages', [])


def build_doc(language, dest_dir):
    os.environ['SPHINXOPTS'] = "-D language='{}'".format(language)
    subprocess.run(f"sphinx-build -b html . {dest_dir}", shell=True)


build_doc("en", "../public")
for language in languages:
    build_doc(language['code'], f"../public/{language['code']}")
