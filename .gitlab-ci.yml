image: python:3.11-bookworm

stages:
  - test
  - build
  - deploy

test:bandit:
  script:
    - cp config.yaml.example config.yaml
    - apt update && apt install build-essential
    - pip install -r requirements.txt --quiet
    - pip install bandit --quiet
    - bandit -r app

test:docs:
  stage: test
  script:
    - cp config.yaml.example config.yaml
    - apt update && apt install build-essential
    - pip install -r requirements.txt
    - pip install -r requirements-docs.txt
    - pushd docs && DISABLE_METRICS=true python build_docs.py && popd
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

test:flake8:
  script:
    - cp config.yaml.example config.yaml
    - apt update && apt install build-essential
    - pip install -r requirements.txt --quiet
    - pip install flake8 --quiet
    - flake8 app

test:mypy:
  script:
    - cp config.yaml.example config.yaml
    - apt update && apt install build-essential
    - pip install -r requirements.txt --quiet
    - pip install -r requirements-types.txt --quiet
    - mypy app

test:pytest:
  script:
    - cp config.yaml.example config.yaml
    - apt update && apt install build-essential
    - pip install -r requirements.txt --quiet
    - pip install pytest
    - DISABLE_METRICS=true python -m pytest tests

test:pylint:
  script:
    - cp config.yaml.example config.yaml
    - apt update && apt install build-essential
    - pip install -r requirements.txt --quiet
    - pip install pylint --quiet
    - pylint app
  allow_failure: true

test:migration:
  script:
    - cp config.yaml.example config.yaml
    - pip install -r requirements.txt --quiet
    - sh scripts/test_migrate.sh

pages:
  stage: deploy
  script:
    - cp config.yaml.example config.yaml
    - apt update && apt install build-essential
    - pip install -r requirements.txt
    - pip install -r requirements-docs.txt
    - pushd docs && DISABLE_METRICS=true python build_docs.py && popd
  artifacts:
    paths:
    - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH


docker-build:
  image: docker:latest
  stage: build
  allow_failure: true
  services:
    - docker:dind
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  # Default branch leaves tag empty (= latest tag)
  # All other branches are tagged with the escaped branch name (commit ref slug)
  script:
    - |
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        tag=""
        echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = 'latest'"
      else
        tag=":$CI_COMMIT_REF_SLUG"
        echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
      fi
    - docker build --pull -t "$CI_REGISTRY_IMAGE${tag}" .
    - docker push "$CI_REGISTRY_IMAGE${tag}"
  # Run this job in a branch where a Dockerfile exists
  rules:
    - if: $CI_COMMIT_BRANCH
      exists:
        - Dockerfile
