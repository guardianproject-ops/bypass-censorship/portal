"""Abstracting cloud providers

Revision ID: 431704bac57a
Revises: 89a74e347d85
Create Date: 2023-01-27 13:32:39.933555

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '431704bac57a'
down_revision = '89a74e347d85'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('cloud_account',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('description', sa.String(length=255), nullable=False),
                    sa.Column('added', sa.DateTime(), nullable=False),
                    sa.Column('updated', sa.DateTime(), nullable=False),
                    sa.Column('destroyed', sa.DateTime(), nullable=True),
                    sa.Column('provider',
                              sa.Enum('AWS', 'AZURE', 'BUNNY', 'CLOUDFLARE', 'FASTLY', 'HTTP', 'GANDI', 'GITHUB',
                                      'GITLAB', 'HCLOUD', 'MAXMIND', 'OVH', 'RFC2136', name='cloudprovider'),
                              nullable=True),
                    sa.Column('credentials', sa.JSON(), nullable=True),
                    sa.Column('enabled', sa.Boolean(), nullable=True),
                    sa.Column('max_distributions', sa.Integer(), nullable=True),
                    sa.Column('max_sub_distributions', sa.Integer(), nullable=True),
                    sa.Column('max_instances', sa.Integer(), nullable=True),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_cloud_account'))
                    )
    with op.batch_alter_table('bridge', schema=None) as batch_op:
        batch_op.add_column(sa.Column('cloud_account_id', sa.Integer(), nullable=True))
        batch_op.create_foreign_key(batch_op.f('fk_bridge_cloud_account_id_cloud_account'), 'cloud_account',
                                    ['cloud_account_id'], ['id'])
        batch_op.drop_column('provider')


def downgrade():
    op.drop_table('bridge')  # We can't guess at what the old providers would have been easily
    op.create_table('bridge',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('added', sa.DateTime(), nullable=False),
                    sa.Column('updated', sa.DateTime(), nullable=False),
                    sa.Column('deprecated', sa.DateTime(), nullable=True),
                    sa.Column('deprecation_reason', sa.String(), nullable=True),
                    sa.Column('destroyed', sa.DateTime(), nullable=True),
                    sa.Column('conf_id', sa.Integer(), nullable=False),
                    sa.Column('provider', sa.String(), nullable=False),
                    sa.Column('terraform_updated', sa.DateTime(), nullable=True),
                    sa.Column('nickname', sa.String(length=255), nullable=True),
                    sa.Column('fingerprint', sa.String(length=255), nullable=True),
                    sa.Column('hashed_fingerprint', sa.String(length=255), nullable=True),
                    sa.Column('bridgeline', sa.String(length=255), nullable=True),
                    sa.ForeignKeyConstraint(['conf_id'], ['bridge_conf.id'],
                                            name=op.f('fk_bridge_conf_id_bridge_conf')),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_bridge'))
                    )
    op.drop_table('cloud_account')
