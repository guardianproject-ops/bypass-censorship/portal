from app.terraform.list import ListAutomation


class ListS3Automation(ListAutomation):
    short_name = "list_s3"
    description = "Update mirror lists in AWS S3 buckets"
    provider = "s3"

    template_parameters = ["aws_access_key", "aws_secret_key"]

    template = """
    terraform {
      {{ backend_config }}
      required_providers {
        aws = {
          version = "~> 4.4.0"
        }
      }
    }

    {% for list in lists %}
    provider "aws" {
      access_key = "{{ aws_access_key }}"
      secret_key = "{{ aws_secret_key }}"
      region = "{{ list.branch }}"
      {% if list.role %}
      assume_role {
        role_arn = "{{ list.role }}"
      }
      {% endif %}
      alias = "list_{{ list.id }}"
    }

    resource "aws_s3_object" "object_{{ list.id }}" {
      provider            = aws.list_{{ list.id }}
      bucket              = "{{ list.container }}"
      key                 = "{{ list.filename }}"
      source              = "{{ list.format }}.{{ list.pool.pool_name }}.{{ list.encoding }}"
      content_type        = "application/json"
      etag                = filemd5("{{ list.format }}.{{ list.pool.pool_name }}.{{ list.encoding }}")
    }
    {% endfor %}
    """
