import os
from waitress import serve
from paste.translogger import TransLogger
from app.__init__ import app
from flask_migrate import upgrade

# equivalent of running flask db upgrade
with app.app_context():
    upgrade()

serve(TransLogger(app), host=os.environ["WAITRESS_RUN_HOST"], port=os.environ["WAITRESS_RUN_PORT"], threads=int(os.environ["WAITRESS_THREADS"]))
