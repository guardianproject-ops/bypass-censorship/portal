=====================
Short Link Redirector
=====================

Welcome to the documentation for the *Bypass Censorship's Short Link Redirector* application. This documentation provides detailed information about the features, components, and usage of the application. The Short Link Redirector is a web application that allows users to generate short links and direct mirror links for URLs.

Features
--------

The Short Link Redirector application offers the following features:

- Short Link Generation: Users can generate short links for URLs, making them easier to share and remember.
- Direct Mirror Links: Users can generate and discover direct mirror links that are currently live.
- Country-based Redirect: The application can redirect users to mirror servers based on their country, improving user experience.
- API Key Authentication: Users can authenticate their requests using API keys, enabling access to mirrors in private resource pools.
- GeoIP Integration: The application optionally leverages GeoIP data to perform country lookups for IP addresses.

Components
----------

The Short Link Redirector application consists of the following main components:

- Flask Web Server: The web server that hosts the application and handles incoming HTTP requests.
- SQLAlchemy: The ORM (Object-Relational Mapping) library used for interacting with the database.
- GeoIP Database: The MaxMind GeoIP database that provides country information for IP addresses.
- Hashids: The library used for encoding and decoding the short link hashes.

Documentation Structure
-----------------------

This documentation is structured into the following sections:

1. :doc:`Installation <install>`: Provides instructions for installing and setting up the Short Link Redirector application.
2. :doc:`Configuration <config>`: Explains the configuration options and how to customize the application's behavior.

We recommend reading through the documentation sequentially to gain a comprehensive understanding of the application and its features.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   install
   config
