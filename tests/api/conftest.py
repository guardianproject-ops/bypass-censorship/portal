import os
import shutil
import tempfile
from datetime import datetime, timezone
from multiprocessing import Process
from time import sleep

import pytest
from sqlalchemy.exc import IntegrityError

from app import app, db
from app.models.base import Group


@pytest.fixture(scope="session", autouse=True)
def test_server(test_database):
    process = Process(target=run_app)
    process.start()

    sleep(2)

    yield

    process.terminate()
    process.join()


@pytest.fixture(scope="session", autouse=True)
def test_database():
    temp_db = tempfile.NamedTemporaryFile(suffix=".db", delete=False)
    db_path = temp_db.name

    # app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:///{db_path}"
    app.config["TESTING"] = True

    with app.app_context():
        db.create_all()
        group = Group(group_name="test-group",
                      description="Test group",
                      eotk=True, added=datetime.now(timezone.utc),
                      updated=datetime.now(timezone.utc))
        try:
            db.session.add(group)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()

    yield db_path

    with app.app_context():
        db.drop_all()

    os.unlink(db_path)


def run_app():
    app.run(host="localhost", port=5001, debug=False, use_reloader=False)


@pytest.fixture(scope="function")
def temporary_test_directory():
    temp_dir = tempfile.mkdtemp()
    yield temp_dir
    shutil.rmtree(temp_dir)  # Cleanup after the test
