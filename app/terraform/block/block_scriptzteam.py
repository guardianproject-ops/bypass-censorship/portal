import requests

from app.terraform.block.bridge_bridgelines import BlockBridgelinesAutomation


class BlockBridgeScriptzteamAutomation(BlockBridgelinesAutomation):
    """
    Automation task to rotate bridges discovered by scriptzteam.
    """

    short_name = "block_bridge_scriptzteam"
    description = "Import bridges discovered by scriptzteam from GitHub"
    frequency = 300  # They only update every couple of days as of February 2023

    def fetch(self) -> None:
        r = requests.get(
            "https://raw.githubusercontent.com/scriptzteam/Tor-Bridges-Collector/main/bridges-obfs4",
            timeout=60,
        )
        r.encoding = "utf-8"
        contents = r.text
        self._lines = contents.splitlines()
