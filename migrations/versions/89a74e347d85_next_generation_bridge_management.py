"""next generation bridge management

Revision ID: 89a74e347d85
Revises: 09b5f4bd75b8
Create Date: 2023-01-25 12:51:28.620426

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '89a74e347d85'
down_revision = '09b5f4bd75b8'
branch_labels = None
depends_on = None


def upgrade():
    bind = op.get_bind()
    if bind.engine.name != 'sqlite':
        op.drop_constraint('fk_bridge_conf_id_bridge_conf', 'bridge')
        op.drop_constraint('fk_bridge_conf_group_id_group', 'bridge_conf')
        op.drop_constraint('pk_bridge', 'bridge')
        op.drop_constraint('pk_bridge_conf', 'bridge_conf')
    op.rename_table('bridge', 'original_bridge')
    op.rename_table('bridge_conf', 'original_bridge_conf')
    op.create_table('bridge_conf',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('description', sa.String(length=255), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=False),
    sa.Column('destroyed', sa.DateTime(), nullable=True),
    sa.Column('pool_id', sa.Integer(), nullable=False),
    sa.Column('method', sa.String(length=20), nullable=False),
    sa.Column('target_number', sa.Integer(), nullable=True),
    sa.Column('max_number', sa.Integer(), nullable=True),
    sa.Column('expiry_hours', sa.Integer(), nullable=True),
    sa.Column('provider_allocation', sa.Enum('RANDOM', 'COST', name='providerallocation'), nullable=True),
    sa.ForeignKeyConstraint(['pool_id'], ['pool.id'], name=op.f('fk_bridge_conf_pool_id_pool')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_bridge_conf'))
    )
    op.create_table('bridge',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('added', sa.DateTime(), nullable=False),
    sa.Column('updated', sa.DateTime(), nullable=False),
    sa.Column('deprecated', sa.DateTime(), nullable=True),
    sa.Column('deprecation_reason', sa.String(), nullable=True),
    sa.Column('destroyed', sa.DateTime(), nullable=True),
    sa.Column('conf_id', sa.Integer(), nullable=False),
    sa.Column('provider', sa.String(), nullable=False),
    sa.Column('terraform_updated', sa.DateTime(), nullable=True),
    sa.Column('nickname', sa.String(length=255), nullable=True),
    sa.Column('fingerprint', sa.String(length=255), nullable=True),
    sa.Column('hashed_fingerprint', sa.String(length=255), nullable=True),
    sa.Column('bridgeline', sa.String(length=255), nullable=True),
    sa.ForeignKeyConstraint(['conf_id'], ['bridge_conf.id'], name=op.f('fk_bridge_conf_id_bridge_conf')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_bridge'))
    )


def downgrade():
    bind = op.get_bind()
    op.drop_table('bridge')
    op.drop_table('bridge_conf')
    op.rename_table('original_bridge_conf', 'bridge_conf')
    if bind.engine.name != 'sqlite':
        op.create_primary_key('pk_bridge_conf', 'bridge_conf', ['id'])
        op.create_foreign_key('fk_bridge_conf_group_id_group', 'bridge_conf', 'group', ['group_id'], ['id'])
    op.rename_table('original_bridge', 'bridge')
    if bind.engine.name != 'sqlite':
        op.create_primary_key('pk_bridge', 'bridge', ['id'])
        op.create_foreign_key('fk_bridge_conf_id_bridge_conf', 'bridge', 'bridge_conf', ['conf_id'], ['id'])
