==================
Configuration File
==================

The application uses a YAML configuration file to specify various settings. The configuration file should follow the YAML syntax and provide values for the following fields:

- `DEFAULT_REDIRECTOR_DOMAIN`: The default domain name to use for generating short links when no pool-specific redirector domain is provided. If not specified, it defaults to an empty string.
- `MIRROR_COUNTRIES`: A list of country codes for which clients should be redirected to a mirror server. If a client's IP address matches one of the specified countries, they will be redirected. If not specified, clients will always be redirected to a mirror server.
- `PUBLIC_KEY`: The public API key used for generating short links for anonymous users. If not specified, it defaults to an empty string.
- `UPDATE_KEY`: The API key required for updating the application data via the `/updt` endpoint. This key must be provided in the `Authorization` header as a bearer token.
- `GEOIP_DATABASE` (optional): The file path to the GeoIP database used for country lookup. If not specified, it defaults to `/usr/share/GeoIP/GeoIP2-Country.mmdb`.

Example Configuration
---------------------

An example configuration file may look like this:

.. code-block:: yaml

    DEFAULT_REDIRECTOR_DOMAIN: example.com
    MIRROR_COUNTRIES:
      - US
      - CA
      - GB
    PUBLIC_KEY: public_key
    GEOIP_DATABASE: /path/to/geoip/database.mmdb