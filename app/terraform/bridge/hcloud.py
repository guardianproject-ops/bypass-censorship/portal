from app.models.cloud import CloudProvider
from app.terraform.bridge import BridgeAutomation


class BridgeHcloudAutomation(BridgeAutomation):
    short_name = "bridge_hcloud"
    description = "Deploy Tor bridges on Hetzner Cloud"
    provider = CloudProvider.HCLOUD

    template_parameters = ["ssh_private_key_path", "ssh_public_key_path"]

    template = """
    terraform {
      {{ backend_config }}
      required_providers {
        random = {
          source = "hashicorp/random"
          version = "3.1.0"
        }
        hcloud = {
          source  = "hetznercloud/hcloud"
          version = "1.31.1"
        }
      }
    }

    locals {
      ssh_private_key = "{{ ssh_private_key_path }}"
    }

    {% for resource in destroyed_resources %}
    {% set bridge, bridgeconf, account = resource %}
    provider "hcloud" {
      token = "{{ account.credentials["hcloud_token"] }}"
      alias = "account_{{ bridge.id }}"
    }
    {% endfor %}

    {% for resource in active_resources %}
    {% set bridge, bridgeconf, account = resource %}
    provider "hcloud" {
      token = "{{ account.credentials["hcloud_token"] }}"
      alias = "account_{{ bridge.id }}"
    }

    data "hcloud_datacenters" "ds_{{ bridge.id }}" {
      provider = hcloud.account_{{ bridge.id }}
    }

    data "hcloud_server_type" "cx22_{{ bridge.id }}" {
      provider = hcloud.account_{{ bridge.id }}
      name = "cx22"
    }

    resource "random_shuffle" "datacenter_{{ bridge.id }}" {
      input = [for s in data.hcloud_datacenters.ds_{{ bridge.id }}.datacenters : s.name if contains(s.available_server_type_ids, data.hcloud_server_type.cx22_{{ bridge.id }}.id)]
      result_count = 1

      lifecycle {
        ignore_changes = [input] # don't replace all the bridges if a new DC appears
      }
    }

    module "bridge_{{ bridge.id }}" {
      providers = {
        hcloud = hcloud.account_{{ bridge.id }}
      }
      source = "{{ terraform_modules_path }}/terraform-hcloud-tor-bridge"
      datacenter = one(random_shuffle.datacenter_{{ bridge.id }}.result)
      namespace = "{{ global_namespace }}"
      name = "bridge"
      attributes = ["{{ bridge.id }}"]
      ssh_private_key = local.ssh_private_key
      contact_info = "this used to be sanitised and I did not write the code to populate it yet"
      distribution_method = "{{ bridgeconf.method }}"
    }

    output "bridge_hashed_fingerprint_{{ bridge.id }}" {
      value = module.bridge_{{ bridge.id }}.hashed_fingerprint
    }

    output "bridge_bridgeline_{{ bridge.id }}" {
      value = module.bridge_{{ bridge.id }}.bridgeline
      sensitive = true
    }
    {% endfor %}
    """
