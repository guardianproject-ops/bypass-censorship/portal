from flask_wtf import FlaskForm
from wtforms import SelectField, StringField, SubmitField


class EditMirrorForm(FlaskForm):  # type: ignore
    origin = SelectField("Origin")
    url = StringField("URL")
    submit = SubmitField("Save Changes")
