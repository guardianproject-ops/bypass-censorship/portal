import json

import pytest

from app.terraform.list import obfuscator


@pytest.mark.parametrize("input_", ["hello", {"hello": "world"}, ["hello", "world"]])
def test_obfuscator_simple(input_):
    obfuscated = obfuscator(input_)
    output = json.dumps(obfuscated)
    loaded = json.loads(output.replace("!AAA!", "\\u"))
    assert loaded == input_


def test_obfuscator_for_real():
    input_ = json.load(open("tests/list/mirrorSites.json"))
    obfuscated = obfuscator(input_)
    output = json.dumps(obfuscated)
    allowed_characters = ["!", "A", ",", " ", "{", "}", "[", "]", ":", '"', chr(13)]
    allowed_characters.extend([chr(x) for x in range(ord("0"), ord("9") + 1)])
    allowed_characters.extend([chr(x) for x in range(ord("a"), ord("f") + 1)])
    for c in output:
        assert c in allowed_characters
    loaded = json.loads(output.replace("!AAA!", "\\u"))
    assert loaded == input_
