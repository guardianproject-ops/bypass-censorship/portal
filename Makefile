SHELL := /bin/bash

export README_TEMPLATE_FILE ?= $(BUILD_HARNESS_EXTENSIONS_PATH)/templates/README.md.gotmpl
export README_DEPS ?= docs/targets.md

-include $(shell curl -sSL -o .build-harness-ext "https://go.sr2.uk/build-harness"; echo .build-harness-ext)

.PHONY: install-frontend
# Clone the frontend repo, install dependencies, and build the React app for serving with Flask
install-frontend:
	if [ ! -d "frontend" ]; then git clone https://gitlab.com/guardianproject-ops/bypass-censorship/portal-frontend.git frontend; fi
	cd frontend && npm install && npm run build
	mkdir -p app/static/ui
	cp -r frontend/dist/* app/static/ui/
