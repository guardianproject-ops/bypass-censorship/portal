import logging
import secrets
from datetime import datetime, timezone

import sqlalchemy
from flask import Blueprint, Response, flash, redirect, render_template, url_for
from flask.typing import ResponseReturnValue
from flask_wtf import FlaskForm
from wtforms import SelectField, StringField, SubmitField
from wtforms.validators import DataRequired

from app.extensions import db
from app.models.base import Group, Pool
from app.portal.util import LifecycleForm

bp = Blueprint("pool", __name__)


class NewPoolForm(FlaskForm):  # type: ignore[misc]
    group_name = StringField("Short Name", validators=[DataRequired()])
    description = StringField("Description", validators=[DataRequired()])
    redirector_domain = StringField("Redirector Domain")
    submit = SubmitField("Save Changes", render_kw={"class": "btn btn-success"})


class EditPoolForm(FlaskForm):  # type: ignore[misc]
    description = StringField("Description", validators=[DataRequired()])
    redirector_domain = StringField("Redirector Domain")
    api_key = StringField(
        "API Key",
        description=(
            "Any change to this field (e.g. clearing it) will result in the "
            "API key being regenerated."
        ),
    )
    submit = SubmitField("Save Changes", render_kw={"class": "btn btn-success"})


class GroupSelectForm(FlaskForm):  # type: ignore[misc]
    group = SelectField("Group", validators=[DataRequired()])
    submit = SubmitField("Save Changes", render_kw={"class": "btn btn-success"})


@bp.route("/list")
def pool_list() -> ResponseReturnValue:
    pools = Pool.query.order_by(Pool.pool_name).all()
    return render_template(
        "list.html.j2",
        section="pool",
        title="Resource Pools",
        item="pool",
        items=pools,
        new_link=url_for("portal.pool.pool_new"),
    )


@bp.route("/new", methods=["GET", "POST"])
def pool_new() -> ResponseReturnValue:
    form = NewPoolForm()
    if form.validate_on_submit():
        pool = Pool()
        pool.pool_name = form.group_name.data
        pool.description = form.description.data
        pool.redirector_domain = (
            form.redirector_domain.data if form.redirector_domain.data != "" else None
        )
        pool.api_key = secrets.token_urlsafe(nbytes=32)
        pool.added = datetime.now(timezone.utc)
        pool.updated = datetime.now(timezone.utc)
        try:
            db.session.add(pool)
            db.session.commit()
            flash(f"Created new pool {pool.pool_name}.", "success")
            return redirect(url_for("portal.pool.pool_edit", pool_id=pool.id))
        except sqlalchemy.exc.SQLAlchemyError as exc:
            flash("Failed to create new pool.", "danger")
            logging.exception(exc)
            return redirect(url_for("portal.pool.pool_list"))
    return render_template("new.html.j2", section="pool", form=form)


@bp.route("/edit/<pool_id>", methods=["GET", "POST"])
def pool_edit(pool_id: int) -> ResponseReturnValue:
    pool = Pool.query.filter(Pool.id == pool_id).first()
    if pool is None:
        return Response(
            render_template(
                "error.html.j2",
                section="pool",
                header="404 Pool Not Found",
                message="The requested pool could not be found.",
            ),
            status=404,
        )
    form = EditPoolForm(
        description=pool.description,
        api_key=pool.api_key,
        redirector_domain=pool.redirector_domain,
    )
    if form.validate_on_submit():
        pool.description = form.description.data
        pool.redirector_domain = (
            form.redirector_domain.data if form.redirector_domain.data != "" else None
        )
        if form.api_key.data != pool.api_key:
            pool.api_key = secrets.token_urlsafe(nbytes=32)
            form.api_key.data = pool.api_key
        pool.updated = datetime.now(timezone.utc)
        try:
            db.session.commit()
            flash("Saved changes to pool.", "success")
        except sqlalchemy.exc.SQLAlchemyError:
            flash("An error occurred saving the changes to the pool.", "danger")
    return render_template("pool.html.j2", section="pool", pool=pool, form=form)


@bp.route("/group_remove/<pool_id>/<group_id>", methods=["GET", "POST"])
def pool_group_remove(pool_id: int, group_id: int) -> ResponseReturnValue:
    pool = Pool.query.filter(Pool.id == pool_id).first()
    if pool is None:
        return Response(
            render_template(
                "error.html.j2",
                section="pool",
                header="404 Pool Not Found",
                message="The requested pool could not be found.",
            ),
            status=404,
        )
    group = Group.query.filter(Group.id == group_id).first()
    if group is None:
        return Response(
            render_template(
                "error.html.j2",
                section="pool",
                header="404 Group Not Found",
                message="The requested group could not be found.",
            ),
            status=404,
        )
    if group not in pool.groups:
        return Response(
            render_template(
                "error.html.j2",
                section="pool",
                header="404 Group Not In Pool",
                message="The requested group could not be found in the specified pool.",
            ),
            status=404,
        )
    form = LifecycleForm()
    if form.validate_on_submit():
        pool.groups.remove(group)
        try:
            db.session.commit()
            flash("Saved changes to pool.", "success")
            return redirect(url_for("portal.pool.pool_edit", pool_id=pool.id))
        except sqlalchemy.exc.SQLAlchemyError:
            flash("An error occurred saving the changes to the pool.", "danger")
    return render_template(
        "lifecycle.html.j2",
        header=f"Remove {group.group_name} from the {pool.pool_name} pool?",
        message="Resources deployed and available in the pool will be destroyed soon.",
        section="pool",
        pool=pool,
        form=form,
    )


@bp.route("/group_add/<pool_id>", methods=["GET", "POST"])
def pool_group_add(pool_id: int) -> ResponseReturnValue:
    pool = Pool.query.filter(Pool.id == pool_id).first()
    if pool is None:
        return Response(
            render_template(
                "error.html.j2",
                section="pool",
                header="404 Pool Not Found",
                message="The requested pool could not be found.",
            ),
            status=404,
        )
    form = GroupSelectForm()
    form.group.choices = [(x.id, x.group_name) for x in Group.query.all()]
    if form.validate_on_submit():
        group = Group.query.filter(Group.id == form.group.data).first()
        if group is None:
            return Response(
                render_template(
                    "error.html.j2",
                    section="pool",
                    header="404 Group Not Found",
                    message="The requested group could not be found.",
                ),
                status=404,
            )
        pool.groups.append(group)
        try:
            db.session.commit()
            flash("Saved changes to pool.", "success")
            return redirect(url_for("portal.pool.pool_edit", pool_id=pool.id))
        except sqlalchemy.exc.SQLAlchemyError:
            flash("An error occurred saving the changes to the pool.", "danger")
    return render_template(
        "lifecycle.html.j2",
        header=f"Add a group to {pool.pool_name}",
        message="Resources will shortly be deployed and available for all origins in this group.",
        section="pool",
        pool=pool,
        form=form,
    )
