import argparse
import logging
import sys
from os.path import basename
from typing import List

from app.cli.automate import AutomateCliHandler
from app.cli.db import DbCliHandler
from app.cli.list import ListCliHandler


def parse_args(argv: List[str]) -> None:
    if basename(argv[0]) == "__main__.py":
        argv[0] = "bypass"
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v", "--verbose", help="increase logging verbosity", action="store_true"
    )
    subparsers = parser.add_subparsers(title="command", help="command to run")
    AutomateCliHandler.add_subparser_to(subparsers)
    DbCliHandler.add_subparser_to(subparsers)
    ListCliHandler.add_subparser_to(subparsers)
    args = parser.parse_args(argv[1:])
    if "cls" in args:
        command = args.cls(args)
        command.run()
    else:
        parser.print_help()


if __name__ == "__main__":
    VERBOSE = "-v" in sys.argv or "--verbose" in sys.argv
    logging.basicConfig(level=logging.DEBUG if VERBOSE else logging.INFO)
    logging.debug("Arguments: %s", sys.argv)
    parse_args(sys.argv)
