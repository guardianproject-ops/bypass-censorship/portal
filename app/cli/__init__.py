import argparse
from abc import abstractmethod
from typing import TYPE_CHECKING, Any

if TYPE_CHECKING:
    _SubparserType = argparse._SubParsersAction[
        argparse.ArgumentParser
    ]  # pylint: disable=protected-access
else:
    _SubparserType = Any


class BaseCliHandler:
    def __init__(self, args: argparse.Namespace) -> None:
        self.args = args

    @classmethod
    @abstractmethod
    def add_subparser_to(cls, subparsers: _SubparserType) -> None:
        raise NotImplementedError

    @abstractmethod
    def run(self) -> None:
        raise NotImplementedError
