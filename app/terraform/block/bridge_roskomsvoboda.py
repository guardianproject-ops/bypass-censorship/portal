from typing import Any

import requests

from app.terraform.block.bridge import BlockBridgeAutomation


class BlockBridgeRoskomsvobodaAutomation(BlockBridgeAutomation):
    short_name = "block_bridge_rks"
    description = "Import the RosKomSvoboda IP blocklist"
    frequency = 300

    _data: Any

    def fetch(self) -> None:
        self._data = requests.get(
            "https://reestr.rublacklist.net/api/v3/ips/", timeout=180
        ).json()

    def parse(self) -> None:
        self.ips.extend(self._data)
