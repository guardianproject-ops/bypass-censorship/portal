import datetime
from abc import ABC
from typing import List

from dateutil.parser import isoparse

from app.terraform.block.bridge import BlockBridgeAutomation


class BlockBridgeReachabilityAutomation(BlockBridgeAutomation, ABC):

    _lines: List[str]

    def parse(self) -> None:
        for line in self._lines:
            parts = line.split("\t")
            if isoparse(parts[2]) < (
                datetime.datetime.now(datetime.timezone.utc)
                - datetime.timedelta(days=3)
            ):
                # Skip results older than 3 days
                continue
            if int(parts[1]) < 40:
                self.hashed_fingerprints.append(parts[0])
