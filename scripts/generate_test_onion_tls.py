import base64
import json
import os
from datetime import datetime, timedelta, timezone

from tests.api.test_onion import generate_onion_keys_with_mkp224o, generate_self_signed_tls_certificate


def generate_create_rest_payload(parent_folder: str, folder_name: str):
    """
    Generate REST payload for a specific Onion service and append it to a shared .rest file.
    """
    rest_file_path = os.path.join(parent_folder, "new_onion.rest")

    with open(os.path.join(folder_name, "hs_ed25519_secret_key"), "rb") as f:
        onion_private_key = base64.b64encode(f.read()).decode("utf-8")
    with open(os.path.join(folder_name, "hs_ed25519_public_key"), "rb") as f:
        onion_public_key = base64.b64encode(f.read()).decode("utf-8")
    with open(os.path.join(folder_name, "tls_private_key.pem"), "r") as f:
        tls_private_key = f.read()
    with open(os.path.join(folder_name, "tls_certificate.pem"), "r") as f:
        tls_public_key = f.read()

    payload = {
        "DomainName": "example.com",
        "Description": f"Generated Onion Service for {folder_name}",
        "OnionPrivateKey": onion_private_key,
        "OnionPublicKey": onion_public_key,
        "TlsPrivateKey": tls_private_key,
        "TlsCertificate": tls_public_key,
        "SkipChainVerification": True,
        "GroupId": 1,
    }

    with open(rest_file_path, "a") as f:
        f.write(f"### Create Onion Service ({folder_name})\n")
        f.write("POST http://localhost:5000/api/onion/onion\n")
        f.write("Content-Type: application/json\n\n")
        json.dump(payload, f, indent=4)
        f.write("\n\n")


if __name__ == "__main__":
    parent_folder = "."
    scenarios = [
        ("self_signed_onion_service", datetime.now(timezone.utc), datetime.now(timezone.utc) + timedelta(days=365), None),
        ("expired_onion_service", datetime.now(timezone.utc) - timedelta(days=730), datetime.now(timezone.utc) - timedelta(days=365), None),
        ("future_onion_service", datetime.now(timezone.utc) + timedelta(days=365), datetime.now(timezone.utc) + timedelta(days=730), None),
        ("wrong_name_onion_service", datetime.now(timezone.utc), datetime.now(timezone.utc) + timedelta(days=365), ["wrong-name.example.com"]),
    ]

    if os.path.exists("new_onion.rest"):
        os.remove("new_onion.rest")

    for folder_name, valid_from, valid_to, dns_names in scenarios:
        print(f"Generating {folder_name}...")
        onion_address = generate_onion_keys_with_mkp224o(folder_name, "test")
        generate_self_signed_tls_certificate(folder_name, onion_address, valid_from, valid_to, dns_names)
        generate_create_rest_payload(parent_folder, folder_name)

    print("All Onion services and REST requests generated successfully.")
