============
Installation
============

The application is intended to be deployed in AWS using the
`bc-redirector <https://registry.terraform.io/modules/sr2c/bc-redirector/aws/latest>`_
Terraform module.
This deployment is not managed through the portal itself but may be managed alongside the
IaC that manages the deployment of the portal itself.

Deployment
----------

The Terraform module takes care of setting up multiple entry-points to the redirector allowing for
per-pool redirector domains, and also generating the necessary configuration file for the redirector.
Once deployed, the redirector will not require further manual configuration, although the deployed
EC2 instance will be accessible via AWS Systems Manager Session Manager if required for debugging.

The following Terraform configuration shows an example using the module:

.. code-block::

   module "redirector" {
     providers = {
       aws     = aws,
       aws.acm = aws.acm
     }
     source                    = "sr2c/bc-redirector/aws"
     # TODO: version           = "x.x.x"
     namespace                 = "eg"
     name                      = "redirector"
     default_redirector_domain = "example.com"

     entry_points = [
       "example.com",
       "example.org"
     ]

     public_key   = var.public_key
     secret_key   = random_password.secret_key.result
     update_key   = var.update_key

     maxmind_account_id   = var.maxmind_account_id
     maxmind_licence_key  = var.maxmind_licence_key
   }

The `public_key` must match the API key of the public pool configured within the portal.
The `update_key` may must match the "filename" configured for the distribution list below.

The MaxMind account used will need to have at least a subscription for the GeoIP2 Country
database.
A subscription for GeoIP2 City will include the GeoIP2 Country database.

Data Updates
------------

Once deployed, the redirector must learn about the available mirror resources.
This requires configuring a :doc:`distribution list <../../user/lists>` inside the portal.

* **Resource Pool**: any (ignored)
* **Provider**: HTTP POST
* **Distribution Method**: Redirector Data
* **Encoding**: JSON (Plain)
* **Description**: <*description of the redirector instance*>
* **Container**: \https://<*redirector domain*>/updt
* **Git Branch/Region**: na
* **Role ARN**: <*blank*>
* **Filename**: <*update key*>