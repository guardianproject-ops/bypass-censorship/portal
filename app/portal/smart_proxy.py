from flask import Blueprint, render_template
from flask.typing import ResponseReturnValue
from sqlalchemy import desc

from app.models.mirrors import SmartProxy

bp = Blueprint("smart_proxy", __name__)


@bp.route("/list")
def smart_proxy_list() -> ResponseReturnValue:
    instances = (
        SmartProxy.query.filter(SmartProxy.destroyed.is_(None))
        .order_by(desc(SmartProxy.added))
        .all()
    )
    return render_template(
        "list.html.j2",
        section="smart_proxy",
        title="Smart Proxy Instances",
        item="smart proxy",
        items=instances,
    )
