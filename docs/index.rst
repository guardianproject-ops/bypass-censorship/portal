Documentation Home
==================

.. image:: /_static/home.png
   :width: 800

.. toctree::
   :maxdepth: 2
   :caption: User Guide

   user/index.rst
   user/mirrors.rst
   user/eotk.rst
   user/bridges.rst
   user/lists.rst
   user/automation.rst

.. toctree::
   :maxdepth: 2
   :caption: Publisher Guide

   publisher/api.rst

.. toctree::
   :maxdepth: 2
   :caption: Admin Guide

   admin/install.rst
   admin/conf.rst
   admin/external.rst
   admin/eotk.rst
   admin/api/index.rst

.. toctree::
   :maxdepth: 2
   :caption: Technical Documentation

   tech/index.rst
   tech/conf.rst
   tech/resource.rst
   tech/automation.rst


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Maintained by
-------------

+------------+----------------------------------------------------+
| |logo-gp|  | `Guardian Project <https://guardianproject.info>`_ |
+------------+----------------------------------------------------+

Developed by
------------

+------------+----------------------------------------------------+
| |logo-gp|  | `Guardian Project <https://guardianproject.info>`_ |
+------------+----------------------------------------------------+
| |logo-tor| | `Tor Project <https://www.torproject.org>`_        |
+------------+----------------------------------------------------+

Supported by [*]_
-----------------

+------------+--------------------------------------------------+
| |logo-otf| | `Open Technology Fund <https://opentech.fund/>`_ |
+------------+--------------------------------------------------+



.. |logo-gp| image:: /_static/logo-gp.png
   :height: 50
   :alt: Guardian Project

.. |logo-tor| image:: /_static/logo-tor.png
   :height: 50
   :alt: Tor Project

.. |logo-otf| image:: /_static/logo-otf.png
   :width: 146
   :alt: Open Technology Fund

.. rubric:: Footnotes

.. [*]  Support received does not imply endorsement.
