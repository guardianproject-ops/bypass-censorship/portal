from typing import List, Optional, TypedDict

from sqlalchemy.orm import selectinload

from app.models.base import Pool
from app.models.bridges import Bridge


class BridgelinesDict(TypedDict):
    version: str
    bridgelines: List[str]


def bridgelines(
    pool: Pool, *, distribution_method: Optional[str] = None
) -> BridgelinesDict:
    # Fetch bridges with selectinload for related data
    query = Bridge.query.options(selectinload(Bridge.conf)).filter(
        Bridge.destroyed.is_(None),
        Bridge.deprecated.is_(None),
        Bridge.bridgeline.is_not(None),
    )

    if distribution_method is not None:
        query = query.filter(Bridge.conf.has(distribution_method=distribution_method))

    # Collect bridgelines specific to the pool
    bridgelines = [b.bridgeline for b in query.all() if b.conf.pool_id == pool.id]

    # Return dictionary directly, inlining the previous `to_dict` functionality
    return {"version": "1.0", "bridgelines": bridgelines}
