from flask import Blueprint, Response, current_app, render_template
from flask.typing import ResponseReturnValue
from sqlalchemy import desc

from app.models.base import Group
from app.models.onions import Eotk

bp = Blueprint("eotk", __name__)


_SECTION_TEMPLATE_VARS = {
    "section": "eotk",
    "help_url": "https://bypass.censorship.guide/user/eotk.html",
}


@bp.route("/list")
def eotk_list() -> ResponseReturnValue:
    instances = (
        Eotk.query.filter(Eotk.destroyed.is_(None)).order_by(desc(Eotk.added)).all()
    )
    return render_template(
        "list.html.j2",
        title="EOTK Instances",
        item="eotk",
        items=instances,
        **_SECTION_TEMPLATE_VARS
    )


@bp.route("/conf/<group_id>")
def eotk_conf(group_id: int) -> ResponseReturnValue:
    group = Group.query.filter(Group.id == group_id).first()
    return Response(
        render_template(
            "sites.conf.j2",
            bypass_token=current_app.config["BYPASS_TOKEN"],
            group=group,
        ),
        content_type="text/plain",
    )
