import enum
from typing import TYPE_CHECKING, Any, Dict, List

from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.brm.brn import BRN
from app.extensions import db
from app.models import AbstractConfiguration
from app.models.mirrors import StaticOrigin

if TYPE_CHECKING:
    from app.models.bridges import Bridge


class CloudProvider(enum.Enum):
    AWS = ("aws", "Amazon Web Services")
    AZURE = ("azure", "Microsoft Azure")
    BUNNY = ("bunny", "bunny.net")
    CLOUDFLARE = ("cloudflare", "Cloudflare")
    FASTLY = ("fastly", "Fastly")
    HTTP = ("http", "HTTP")
    GANDI = ("gandi", "Gandi")
    GITHUB = ("github", "GitHub")
    GITLAB = ("gitlab", "GitLab")
    HCLOUD = ("hcloud", "Hetzner Cloud")
    MAXMIND = ("maxmind", "MaxMind")
    OVH = ("ovh", "OVH")
    RFC2136 = ("rfc2136", "RFC2136 DNS Server")

    def __init__(self, key: str, description: str):
        self.key = key
        self.description = description


class CloudAccount(AbstractConfiguration):
    provider: Mapped[CloudProvider]
    credentials: Mapped[Dict[str, Any]] = mapped_column(db.JSON())
    enabled: Mapped[bool]
    # CDN Quotas
    max_distributions: Mapped[int]
    max_sub_distributions: Mapped[int]
    # Compute Quotas
    max_instances: Mapped[int]

    bridges: Mapped[List["Bridge"]] = relationship(
        "Bridge", back_populates="cloud_account"
    )
    statics: Mapped[List["StaticOrigin"]] = relationship(
        "StaticOrigin",
        back_populates="storage_cloud_account",
        foreign_keys=[StaticOrigin.storage_cloud_account_id],
    )

    @property
    def brn(self) -> BRN:
        raise NotImplementedError("No BRN for cloud accounts")
