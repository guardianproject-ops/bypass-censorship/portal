Configuration File
==================

A file named ``config.yaml`` must exist. For specifics about the provider configurations, see
:doc:`External Services <external>`.

Base Configuration
------------------

Template
--------

.. literalinclude:: ../../config.yaml.example
