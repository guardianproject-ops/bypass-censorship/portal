import builtins
import os
import sys
import yaml

sys.path.insert(0, os.path.abspath('..'))

language = 'en'  # default language

locale_dirs = ['./locale/']
gettext_compact = False

templates_path = ['./_templates']

builtins.__sphinx_build__ = True

# Define languages and links in html_context
# this is used with templates in /_templates to extend the default theme,
# adding languages to the sidebar

with open('languages.yaml', 'r') as file:
    config = yaml.safe_load(file)
    languages = config.get('languages', [])

html_context = {
    'languages': [[lang['code'], lang['name']] for lang in languages]
}
# -- Project information -----------------------------------------------------

project = 'Bypass Censorship Portal'
copyright = '2021-202r SR2 Communications Limited'
author = 'Bypass Censorship'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinxcontrib.mermaid',
    'sphinxcontrib.openapi'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'furo'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
