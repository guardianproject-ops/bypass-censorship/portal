from flask import Blueprint, jsonify
from flask.typing import ResponseReturnValue
from werkzeug.exceptions import HTTPException

from app.api.onion import api_onion
from app.api.web import api_web

api = Blueprint("api", __name__)
api.register_blueprint(api_onion, url_prefix="/onion")
api.register_blueprint(api_web, url_prefix="/web")


@api.errorhandler(400)
def bad_request(error: HTTPException) -> ResponseReturnValue:
    response = jsonify({"error": "Bad Request", "message": error.description})
    response.status_code = 400
    return response


@api.errorhandler(401)
def unauthorized(error: HTTPException) -> ResponseReturnValue:
    response = jsonify({"error": "Unauthorized", "message": error.description})
    response.status_code = 401
    return response


@api.errorhandler(404)
def not_found(_: HTTPException) -> ResponseReturnValue:
    response = jsonify(
        {"error": "Not found", "message": "Resource could not be found."}
    )
    response.status_code = 404
    return response


@api.errorhandler(500)
def internal_server_error(_: HTTPException) -> ResponseReturnValue:
    response = jsonify(
        {"error": "Internal Server Error", "message": "An unexpected error occurred."}
    )
    response.status_code = 500
    return response
