Enterprise Onion Toolkit
========================

When EOTK instances are deployed for a group, the following main AWS resources are created:

* An EC2 instance in us-east-2
* An EC2 instance in eu-central-1
* A S3 bucket to hold nginx access logs

The EC2 instances are running Ubuntu 20.04 LTS and are configured with `ssm-agent <https://docs.aws.amazon.com/systems-manager/latest/userguide/ssm-agent.html>`_
and `unattended-upgrades <https://wiki.debian.org/UnattendedUpgrades>`_.

EOTK is automatically cloned at instance creation time from Alec Muffet's
`GitHub repository <https://github.com/alecmuffett/eotk>`_ to ``/home/ubuntu/eotk``.
The script to automatically build and install all dependencies is also executed at the instance creation time.

For full details on the instance configuration, refer to the
`cloud-init user data <https://github.com/sr2c/terraform-aws-eotk/blob/main/templates/user_data.yaml>`_.

Logging in to an instance
-------------------------

Via the console
~~~~~~~~~~~~~~~

Using the AWS console, visit the `Session Manager <https://us-east-2.console.aws.amazon.com/systems-manager/session-manager/sessions?region=us-east-2>`_
in the Systems Manager service.
If using SSO, login before attempting to open the link.

.. image:: /_static/ssm/list.png
   :width: 800

Click "Start Session" to start a new session.

.. image:: /_static/ssm/new.png
   :width: 800

Select the instance from the list, and click "Start Session" again.
Remember that the second instance will be found in the eu-central-1 region.

.. image:: /_static/ssm/shell.png
   :width: 800

Use the following commands to get into the EOTK directory as the correct user::

   sudo -u ubuntu bash
   cd /home/ubuntu/eotk

Via the command line
~~~~~~~~~~~~~~~~~~~~

Begin by discovering the instance ID::

   AWS_REGION=us-east-2 aws ec2 describe-instances

Start an SSM session::

   AWS_REGION=us-east-2 aws ssm start-session --target i-083da3fcf840c4797

To login with SSH via SSM, add the following to your ``~/.ssh/config``:

.. note::

   No user SSH public keys are provisioned to the instance. You will need to add the public key you plan to use to the
   correct user's authorized_keys file using a plain SSM session before attempting to use SSH.

.. code-block::

   Host i-* mi-*
        ProxyCommand sh -c "aws ssm start-session --target %h --document-name AWS-StartSSHSession --parameters 'portNumber=%p'"

You can then login with SSH, but be sure to include environment variables for your AWS region::

   AWS_REGION=us-east-2 ssh ubuntu@i-083da3fcf840c4797

This will also work for scp, rsync, etc.

Configuring EOTK
----------------

Refer to the `EOTK documentation <https://github.com/alecmuffett/eotk/tree/master/docs.d>`_.