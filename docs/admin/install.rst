Installation
============

The current installation method is to use a Python virtual envrionment.
Many of the dependencies needed by the portal can be installed from PyPI, although some
dependencies will need to be satisfied in other ways:

* A cron daemon - probably available in your system package manager
* Terraform binary - static binaries available from Hashicorp
* PostgreSQL server - probably available in your system package manager

Create and activate a virtual environment with:

.. code-block:: shell

   cd /installation/directory
   python -m venv env
   . env/bin/activate

You will need to install an entry into the portal user's crontab::

   */1 * * * * (cd /path/to/portal ; /path/to/env/bin/python -m app.cli automate --all) &>/dev/null
